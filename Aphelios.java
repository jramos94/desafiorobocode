package Lunar;
import robocode.*;
import robocode.ScannedRobotEvent;
import java.awt.Color;
import robocode.util.*;



/**
 * Aphelios - a robot by (your name here)
 */
public class Aphelios extends Robot
{
	/**
	 * run: Aphelios's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setColors(Color.black,Color.pink,Color.black); // body,gun,radar
		setBulletColor(Color.green);
		// Robot main loop
		while(true) {

			moveRobot();			
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		
		double absoluteBearing = getHeading() + e.getBearing();
		double bearingFromGun = Utils.normalRelativeAngleDegrees(absoluteBearing - getGunHeading());

		
		if (Math.abs(bearingFromGun) <= 3) {
			turnGunRight(bearingFromGun);
			
			if (getGunHeat() == 0) {
				fire(Math.min(3 - Math.abs(bearingFromGun), getEnergy() - .1));
			}
		} 
		else {
			turnGunRight(bearingFromGun);
		}
		
		if (bearingFromGun == 0) {
			scan();
		}
	}
	public void onHitByBullet(HitByBulletEvent e) {
		// Recebe um tiro e se movimenta um pouco para trás.
		back(30);
	}
	
	/**
	 * onHitWall: quando se bater na parede.
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(100);
		ahead(200);
		turnRight(75);
		
	}	
	private void moveRobot() {
		turnRadarLeft(360);
		ahead(200);
		turnRight(300);
		turnGunRight(360);
		back(150);
		turnGunLeft(360);
	}
	
	}
